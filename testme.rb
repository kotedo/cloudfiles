require "openssl"

unless ARGV.length == 4
  puts "Syntax: <method> <url> <seconds> <key>"
  puts ("Example: GET https://storage101.dfw1.clouddrive.com/v1/" +
        "MossoCloudFS_12345678-9abc-def0-1234-56789abcdef0/" +
        "container/path/to/object.file 60 my_shared_secret_key")
else
  method, url, seconds, key = ARGV
  method = method.upcase
  base_url, object_path = url.split(/\/v1\//)
  object_path = '/v1/' + object_path
  seconds = seconds.to_i
  expires = (Time.now + seconds).to_i
  hmac_body = "#{method}\n#{expires}\n#{object_path}"
  sig = OpenSSL::HMAC.hexdigest("sha1", key, hmac_body)
  puts ("#{base_url}#{object_path}?" +
        "temp_url_sig=#{sig}&temp_url_expires=#{expires}")
end
