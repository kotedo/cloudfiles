-module(temp_url).
-export([make/4]).

-define(GREGORIAN_SECONDS_1970, 62167219200).

make(Method, Url, SecondsAlive, Key) ->
    code:add_path("/var/www/ChicagoBoss/deps/mochiweb/ebin"),

    M = string:to_upper(Method),
    
    [BaseUrl,OPath] = re:split(Url, "/v1/"),
    ObjectPath = "/v1/" ++ binary_to_list(OPath),
    In = erlang:now(),
    Seconds = calendar:datetime_to_gregorian_seconds(calendar:now_to_local_time(In)),
    Expires = (Seconds + SecondsAlive) - ?GREGORIAN_SECONDS_1970,
    %% HmacBody= io_lib:format("~s~n~b~n~s~n", [M, Expires, ObjectPath]),

    HmacBody = M ++ "~n" ++ integer_to_list(Expires) ++ "~n" ++ ObjectPath,
    Signature = mochihex:to_hex(crypto:sha_mac(HmacBody, Key)),
    io:format("STUFF: ~p~n", [binary_to_list(BaseUrl) ++ ObjectPath ++ "?" ++ "temp_url_sig=" ++ Signature ++ "&temp_url_expires=" ++ integer_to_list(Expires)]), 





    io:format("In  : ~p~n"
	      "Out : ~p~n"
	      "S1  : ~p~n"
	      "S2  : ~p~n"
	      "HMAC: ~p~n", [calendar:datetime_to_gregorian_seconds(calendar:now_to_local_time(In)),
			     calendar:datetime_to_gregorian_seconds(calendar:now_to_local_time(In)) + SecondsAlive,
			     calendar:gregorian_seconds_to_datetime(Seconds),
			     calendar:gregorian_seconds_to_datetime(Seconds + SecondsAlive),
			     HmacBody
			    ]).
    
