%%%-------------------------------------------------------------------
%%% @author Kai Janson <kai.janson@MHM23TDV13>
%%% @copyright (C) 2012, Kai Janson
%%% @doc
%%%
%%% @end
%%% Created :  1 Jul 2012 by Kai Janson <kai.janson@MHM23TDV13>
%%%-------------------------------------------------------------------
-module(cloudfiles).
-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
	 terminate/2, code_change/3]).

-define(SERVER, ?MODULE).
-define(AVATARS, "forum_avatars").
-define(USERIMAGES, "forum_userimages").

-record(state, {auth_token, storage_token, cdn_url, storage_url,
		avatar_ssl_uri, avatar_uri, avatar_streaming_uri, avatar_ios_uri,
		userimage_ssl_uri, userimage_uri, userimage_streaming_uri, userimage_ios_uri
	       }).

%% For create()
-include_lib("kernel/include/file.hrl").

%% Functions for the Cloudfiles API
-export([
	 authenticate/0,
	 create_container/1,
	 list/1,
	 delete/1,
	 create/2,
	 dump/0,

%% Additional functions to retrieve the
%% public images

	 avatar_url/0,
	 avatar_ssl_url/0,
	 avatar_streaming_url/0,
	 avatar_ios_url/0,

	 userimage_url/0,
	 userimage_ssl_url/0,
	 userimage_streaming_url/0,
	 userimage_ios_url/0
	]).

%%%===================================================================
%%% API
%%%===================================================================
authenticate() ->
    gen_server:call(?MODULE, authenticate).

create_container(Name) ->
    gen_server:call(?MODULE, {create_container, Name}).

list(Container) ->
    gen_server:call(?MODULE, {list, Container}).

delete(Container) ->
    gen_server:call(?MODULE, {delete, Container}).

create(Container, Filename) ->
    gen_server:call(?MODULE, {create, Container, Filename}).

dump() ->
    gen_server:call(?MODULE, dump).

%% Additional functions
avatar_url() ->
    gen_server:call(?MODULE, avatar_url).
avatar_ssl_url() ->
    gen_server:call(?MODULE, avatar_ssl_url).
avatar_streaming_url() ->
    gen_server:call(?MODULE, avatar_streaming_url).
avatar_ios_url() ->
    gen_server:call(?MODULE, avatar_ios_url).

userimage_url() ->
    gen_server:call(?MODULE, userimage_url).
userimage_ssl_url() ->
    gen_server:call(?MODULE, userimage_ssl_url).
userimage_streaming_url() ->
    gen_server:call(?MODULE, userimage_streaming_url).
userimage_ios_url() ->
    gen_server:call(?MODULE, userimage_ios_url).

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    application:start(crypto),
    application:start(public_key),
    application:start(ssl),
    application:start(inets),
    application:start(xmerl),
    application:start(mochiweb),
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(authenticate, _From, State) ->
    {ok, {_Result, Headers, _Body}} = httpc:request(get,
					     {rs_cf_url(),
					      [
                                               {"X-Auth-Key", rs_cf_key()},
					       {"X-Auth-User", rs_cf_user()}
                                              ]
                                             }, [], [{sync, true}]),
    Storage = proplists:get_value("x-storage-url", Headers),
    CDNStorage = proplists:get_value("x-cdn-management-url", Headers),
    AuthToken = proplists:get_value("x-auth-token", Headers),
    StorageToken = proplists:get_value("x-storage-token", Headers),

    %% Here we need to check the CDN enabled containers
    %% (forum_avatars, forum_userimages) and store the
    %% proper URL's in our record
    X1 = lists:flatten(io_lib:format("~s/~s", [CDNStorage, ?AVATARS])),
    X2 = lists:flatten(io_lib:format("~s/~s", [CDNStorage, ?USERIMAGES])),

    {ok, {_AvatarUriResult, AvatarUriHeaders, _AvatarUriBody}} = httpc:request(head,
					     { X1 ,
					      [
                                               {"X-Auth-Token", AuthToken}
                                              ]
                                             }, [], [{sync, true}]),
    
    {ok, {_UserImagesUriResult, UserImageUriHeaders, _UserImagesUriBody}} = httpc:request(head,
					     { X2,
					      [
                                               {"X-Auth-Token", AuthToken}
                                              ]
                                             }, [], [{sync, true}]),

    AvatarSslUri = proplists:get_value("x-cdn-ssl-uri", AvatarUriHeaders),
    AvatarUri = proplists:get_value("x-cdn-uri", AvatarUriHeaders),
    AvatarStreamingUri = proplists:get_value("x-cdn-streaming-uri", AvatarUriHeaders),
    AvatarIosUri = proplists:get_value("x-cdn-ios-uri", AvatarUriHeaders),
    
    UserImageSslUri = proplists:get_value("x-cdn-ssl-uri", UserImageUriHeaders),
    UserImageUri = proplists:get_value("x-cdn-uri", UserImageUriHeaders),
    UserImageStreamingUri = proplists:get_value("x-cdn-streaming-uri", UserImageUriHeaders),
    UserImageIosUri = proplists:get_value("x-cdn-ios-uri", UserImageUriHeaders),

    NewState = State#state{auth_token=AuthToken, storage_token=StorageToken, cdn_url=CDNStorage, storage_url=Storage,
			   avatar_ssl_uri = AvatarSslUri,
			   avatar_uri = AvatarUri,
			   avatar_streaming_uri = AvatarStreamingUri,
			   avatar_ios_uri = AvatarIosUri,

			   userimage_ssl_uri = UserImageSslUri,
			   userimage_uri = UserImageUri,
			   userimage_streaming_uri = UserImageStreamingUri,
			   userimage_ios_uri = UserImageIosUri
			  },
    io:format("HEADERS:~n~p~n"
	      "X1: ~p~n"
	      "~nAvatarHeaders: ~p~n"
	      "X2: ~p~n"
	      "~nUserImageHeaders: ~p~n", [Headers, X1, AvatarUriHeaders, X2, UserImageUriHeaders]),
    Reply = ok,
    {reply, Reply, NewState};

%% ----------------------------------------------------------------------------------
%%  @doc Create a file (= upload), either on the bare metal (root folder) or inside a container
%% ----------------------------------------------------------------------------------
handle_call({create, Container, Filename}, _From, State) ->
    Filesize = case file:read_file_info(Filename) of
		   {ok, FileInfo} ->
		       FileInfo#file_info.size;
		   {error, Reason} ->
		       Reason
	       end,
    Location = case Container of
		   [] ->
		       lists:flatten(io_lib:format("~s/~s", [State#state.storage_url, filename:basename(Filename)]));
		   Folder ->
		       lists:flatten(io_lib:format("~s/~s/~s", [State#state.storage_url, Folder, filename:basename(Filename)]))
	       end,
    {ok, FileData} = file:read_file(Filename),
    Md5 = mochihex:to_hex(crypto:md5(FileData)),
    ContentType = mime_lib:mimetype(filename:extension(Filename)),
    error_logger:info_msg("File: ~p~nSize: ~p~nLoca: ~p~nMd5 : ~p~nType: ~p~n", [Filename,Filesize,Location,Md5,ContentType]),

    {ok,{{_Http, Status, _Message},_Headers,_Body}} = httpc:request(put,
                                      {Location,
                                       [
                                        {"X-Auth-Token", State#state.auth_token },
					{"Content-Length", Filesize},
					{"Content-Type", ContentType},
					{"ETag", Md5}
                                       ],
                                       mime_lib:mimetype(filename:extension(Filename)), ""
                                      }, [],[{stream, Filename}]),
    Reply = case Status of
		201 ->
		    %% Created, all is good
		    ok;
		400 ->
		    %% Bad Request: The request cannot be fulfilled due to bad syntax.
		    {error, 400, bad_request};
		401 ->
		    %% Unauthorized: Returned upon authentication failure
		    {error, 401, unauthorized};
		403 ->
		    %% Forbidden: The request was a legal request, but the server is refusing to respond to it
		    {error, 403, forbidden};
		411 ->
		    %% Length required: denotes a missing Content-Length or Content-Type header in the request
		    {error, 411, length_required};
		412 ->
		    %% Precondition failed: The server does not meet one of the preconditions that the requester put on the request
		    {error, 412, precondition_failed};
		413 ->
		    %% Request Entity Too Large: The request is larger than the server is willing or able to process
		    {error, 413, request_entity_to_large};
		417 ->
		    %% Expectation Failed: The server cannot meet the requirements of the Expect request-header field
		    {error, 417, expectation_failed};
		422 ->
		    %% Unprocessable entity: indicates that the MD5 checksum of the data written to the
		    %% storage system does NOT match the (optionally) supplied ETag value
		    {error, 422, unprocessable_entity};
		AnythingElse ->
		    {error, AnythingElse, unknown_error}
	    end,
    {reply, Reply, State};
handle_call({delete, Container}, _From, State) ->
    Location = case Container of
		   [] ->
		       lists:flatten(io_lib:format("~s", [State#state.storage_url]));
		   Folder ->
		       lists:flatten(io_lib:format("~s/~s", [State#state.storage_url, Folder]))
	       end,
    {ok, {{_Http,Status,_Message},_Headers, _Body}} = httpc:request(delete,
								{ Location,
								  [
								   {"X-Auth-Token", State#state.auth_token}
								  ]
								}, [], []),
    %% Was it successful?
    Reply = case Status of
		204 ->
		    ok;
		404 ->
		    {error, not_found};
		409 ->
		    {error, container_not_empty}
	    end,
    {reply, Reply, Status};
handle_call({list, Container}, _From, State) ->
    SL = case Container of
             [] ->
                 lists:flatten(io_lib:format("~s?format=xml", [State#state.storage_url]));
             Container ->
                 lists:flatten(io_lib:format("~s/~s?format=xml", [State#state.storage_url, Container]))
         end,
    {ok,{{_Http, Status, _Message}, _Headers, Body}} = httpc:request(get,
								     {SL,
								      [
								       {"X-Auth-Token", State#state.auth_token}
								      ]
								     }, [], []),
    Reply = case Status of 404 -> {error, not_found}; _Else -> Body end,
    {reply, Reply, State};
handle_call(dump, _From, State) ->
    error_logger:info_msg("State: ~p~n", [State]),
    {reply, ok, State};
handle_call({create_container, Name}, _From, State) ->
    {ok,{{_Http, _Status, Message},_Headers,_Body}} = httpc:request(put,
                                      {State#state.storage_url ++ "/" ++ Name,
                                       [
                                        {"X-Auth-Token", State#state.auth_token }
                                       ],
                                       "text/xml", ""
                                      }, [],[]),
    %% Was it successful?
    Reply = case Message of "Created" -> true; _Else -> false end,
    {reply, Reply, State};

handle_call(avatar_url, _From, State ) ->
    Reply = State#state.avatar_uri,
    {reply, Reply, State};
handle_call(avatar_ssl_url, _From, State ) ->
    Reply = State#state.avatar_ssl_uri,
    {reply, Reply, State};
handle_call(avatar_streaming_url, _From, State ) ->
    Reply = State#state.avatar_streaming_uri,
    {reply, Reply, State};
handle_call(avatar_ios_url, _From, State ) ->
    Reply = State#state.avatar_ios_uri,
    {reply, Reply, State};

handle_call(userimage_url, _From, State ) ->
    Reply = State#state.userimage_uri,
    {reply, Reply, State};
handle_call(userimage_ssl_url, _From, State ) ->
    Reply = State#state.userimage_ssl_uri,
    {reply, Reply, State};
handle_call(userimage_streaming_url, _From, State ) ->
    Reply = State#state.userimage_streaming_uri,
    {reply, Reply, State};
handle_call(userimage_ios_url, _From, State ) ->
    Reply = State#state.userimage_ios_uri,
    {reply, Reply, State}.

%%handle_call(_Request, _From, State) ->
%%    Reply = ok,
%%    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
rs_cf_key_sam() ->
    "63ae7709f54b149ca3e00e8628ef9dae".

%% Returns the Rackspace Cloud File Username.                                                                                                                
rs_cf_user_sam() ->
    "samuelrose".

%% Returns the Rackspace Cloud File URL.                                                                                                                     
rs_cf_url_sam() ->
    "https://auth.api.rackspacecloud.com/v1.0".


%%%===================================================================
%%% Ross
%%%===================================================================
rs_cf_key_ross() ->
    "2211d758a0a84da23977338234d9ccf9".

%% Returns the Rackspace Cloud File Username.                                                                                                                
rs_cf_user_ross() ->
    "denizendiaz".

%% Returns the Rackspace Cloud File URL.                                                                                                                     
rs_cf_url_ross() ->
    "https://auth.api.rackspacecloud.com/v1.0".

%%%===================================================================
%%% KnowledgeManager DEVELOPMENT
%%%===================================================================
rs_cf_key() ->
    "643509cba0447858b099e397d5597d6e".

%% Returns the Rackspace Cloud File Username.                                                                                                                
rs_cf_user() ->
    "knowledgemanager".

%% Returns the Rackspace Cloud File URL.                                                                                                                     
rs_cf_url() ->
    "https://auth.api.rackspacecloud.com/v1.0".
